#   Author:     Drew McKinney
#   Project:    VoiceSynth
#   Description:   
#               A tone synthesizer written in python that accepts 
#               audio wav files as input. The program then 
#               generates a synthesized waveform using frequency and 
#               amplitude information from the wav input.
#
#   Arguments:  filename.wav freq_window denoise_factor
#               
#   Output:     Saves synthesized audio as "synth_out.wav" in current directory

# imports
import math
import numpy as np
import matplotlib.pylab as plt
from scipy.io.wavfile import write, read
import os
import sounddevice as sd
import soundfile as sf
import librosa
import time
import noisereduce as nr
from scipy import signal
import sys
#%matplotlib inline

# parameters
# frequency approximation window
freq_window = 100
# wav file input
filename = 'ooh.wav'
# denoise factor 
denoise_factor = 0.6


#   Function:       extract_freq
#   Description:    extracts most dominant frequency from audio input
#                   between arguments t_start and t_end   
#   Arguments:      
#                   filename: input file in cwd
#                   t_start:  frequency approx window start
#                   t_end:    frequency approx window end
#   Outputs:
#                   freq:     most dominant frequency within 
#                             approx window 
def extract_freq(filename, t_start, t_end):

    #data, sr = load(filename)
    data, sample_rate = librosa.load(filename)
    if data.ndim > 1:
        data = data[:, 0]

    # current data slice
    slice = data[int(t_start * sample_rate / 1000) : int(t_end * sample_rate / 1000) + 1]

    # fft
    l = len(slice)
    x = np.fft.rfft(slice)
    y = np.fft.rfftfreq(l, 1 / sample_rate)

    # get most dominant frequency 
    idx = np.argmax(np.abs(x))
    freq = y[idx]
    return freq


#   Function:       create_sine_wave
#   Description:
#                   generates a sine wave with the parameters given
#   Arguments:
#                   freq:       frequency of generated wave
#                   samp_rate:  sample rate of generated wave
#                   ampl:       ampl of wave
#                   dur:        duration of wave in seconds 
#   output:
#                   sinewave:   generated sinewave with given parameters
#
def create_sine_wave(freq=440, samp_rate=48000, ampl=0.5, dur=0.5):
    # parameters
    cpf = 1 # channels per frame (mono)
    samp_size = 16 # bits (-32767..32767)
    #ampl = 0.5
    duration = dur # duration in seconds
    freq = freq #440 # hz (cycles per second)
    samp_rate = samp_rate #48000 # sample rate, samples per second

    time = np.arange(0, dur, 1/samp_rate)

    sinewave = ampl * np.sin(2*np.pi*freq*time)

    return sinewave 

# command line arguments
# ./voicesynth filename slice_size denoise_factor
n = len(sys.argv)
if n > 0:
    for i in sys.argv[1:]:
        if i == sys.argv[1]:
            #print(i)
            filename = i
        elif i == sys.argv[2]:
            freq_window = int(i)
        elif i == sys.argv[3]:
            denoise_factor = float(i)

#data, sr = load(filename)
data, sample_rate = librosa.load(filename, sr=None)
#data = data[:, 0]
num_slices = int(len(data)/freq_window)

input_dur = data.shape[0]/sample_rate

# extract most dominant frequencies 
freqs = []
slice_len = input_dur/num_slices
for i in range(num_slices):
    freqs.append(extract_freq(filename, (slice_len*i)*1000, (slice_len*i+slice_len)*1000))
    
# generate tones for each frequency extracted with amplitude of corresponding input 
synth_tones = []
for x in range(len(freqs)):
    curr_i = math.floor(x*(len(data)/num_slices))
    src_ampl = data[curr_i]
    synth_tones.append(create_sine_wave(freq=freqs[x], samp_rate=sample_rate, ampl=src_ampl, dur=slice_len))
    
tones_audio = np.array(synth_tones).flatten()

# scale synthesized audio
scaled = (tones_audio / np.max(np.abs(tones_audio)))

# denoise
# using noisereduce 2.0.1
# https://pypi.org/project/noisereduce/
synth_out = nr.reduce_noise(y=scaled, sr=sample_rate, prop_decrease=denoise_factor, chunk_size=1000)

# write output wav file
write('synth_out.wav', rate=sample_rate, data=synth_out)
# play original audio 
sd.play(data)
sd.wait()
# play generated audio
sd.play(synth_out)
sd.wait()

