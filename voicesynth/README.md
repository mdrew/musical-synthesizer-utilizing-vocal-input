Author:     Drew McKinney

Project Name:  VoiceSynth

Project Description:  
This project is a synthesizer built in python with the ability to utilize vocal audio as input to generate interesting synthesized audio. Currently, a wav file is used as input, which is assumed to be vocal audio. This data is processed and the tonal information is saved. The synth uses this tonal data to attempt to reconstruct the original audio input with the synthesized audio. This synthesized audio is saved as a wav file and the program plays the newly generated wav file. 

This project is coming along, but there is more I hope to improve before the final draft. I have learned a lot about some python libraries I haven't used much before this class, including sounddevice. I've also gained experience with DSP. I do have some stretch goals which would be interesting and rewarding to complete if I have enough time. I would like to use a separate wav file input as a sample and pitch shift said sample to construct the new tones. I would also like to add a reverb option to the program to add more tunability to the results. 